var bower_path = '../../bower_components/';

requirejs.config({
    baseUrl: 's/js',  // base path with all js
    paths: {
        // ALIAS TO PLUGINS
        domReady:                       'lib/domReady',
        
        // LIBS
        jquery:                         bower_path +'jquery/dist/jquery', // lib for js
        modernizr:                      'lib/modernizr-custom',
        html5shiv:                      'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min',
        moment:                         bower_path + 'moment/moment',
        // PLUGINS

        // jquery_validate:                bower_path + 'jquery-validation/dist/jquery.validate',
        // svg4everybody:                  bower_path + 'svg4everybody/dist/svg4everybody.min',  // load svg
        // scrollbar:                      bower_path + 'perfect-scrollbar/js/perfect-scrollbar.jquery.min',
        // bs_select:                      bower_path + 'bootstrap-select/dist/js/bootstrap-select',
        // bs_dropdown:                    bower_path + 'bootstrap/js/dropdown',
        // bs_transition:                  bower_path + 'bootstrap/js/transition',
        // bs_modal:                       'plugins/bs/modal', // fixed for multiple modal open, need modal fix

        // SEPARATE
        separate_global:                'separate/global', // detect width && height of window\

        // HELPERS
        resizer:                        'helpers/resizer',
        object_fit:                     'helpers/object-fit',
        bs_modal_fix:                   'helpers/bs_modal_fix',
        // detectmobilebrowser:            'lib/detectmobilebrowser',
        // toggle_blocks:                  'helpers/toggle-blocks',
        // update_columns:                 'helpers/update-columns',
        // bs_modal_center:                'helpers/bsModalCenter',
        // valid:                          'helpers/valid',
        
        // COMPONENTS
        'js-header':                    'components/js-header',
        'js-cart-calendar':             'components/js-cart-calendar',
    },
    shim: {
        'bs_modal': {
            deps: ['bs_modal_fix']
        },
        'object_fit': {
            deps: ['modernizr']
        }
    }
});
var main_js_components = [
    'object_fit',
    'separate_global',
    'html5shiv',
    'modernizr',
    'resizer',
    // 'toggle_blocks',
];
requirejs(['domReady','jquery', 'moment'], function(domReady, $){
    requirejs(main_js_components, function(objectfit) {
        loadComponent({
            name: '.js-header',
            req: ['js-header']
        });
        loadComponent({
            name: '.js-cart-calendar',
            req: ['js-cart-calendar']
        });
        // loadComponent({
        //     name: '.js-form_check',
        //     req: ['valid', 'jquery_validate']
        // });
    });
});